# Container Registry

>注意事项：创建镜像使用的是镜像进行执行，gitlab-runner的Excutor需要注册docker类型的。（gitlab-runner register）

## 1. 功能说明
通过将 Docker 容器注册表集成到 GitLab 中，每个 GitLab 项目都可以拥有自己的空间来存储其 Docker 镜像。您可以查看项目或组的 Container Registry。
转到您的项目或小组。
转到Packages & Registries > Container Registry。


## 2. 操作指南

