package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

    @GetMapping("/")
    String home() {
        return "Spring is here!";
    }

    @GetMapping("/new")
    String newFeature() {
        return "Spring is here! + new feature";
    }

    public static void main(String[] args) {
         
        SpringApplication.run(DemoApplication.class, args);
    }

    public void test(int i, int j, int k, int l, int m) {
        String COMP_user = "user";
        String DB_Password = "password";
        System.out.println("hello world" + COMP_user + DB_Password);
        System.out.println("test");
    }
}
